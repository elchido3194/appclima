//
//  ViewController.swift
//  AppClima
//
//  Created by Jose Diaz on 25/10/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        usuarioTextField.text=""
        passwdTextField.text=""
        usuarioTextField.becomeFirstResponder()
    }
    //MARK:- Outlets
    @IBOutlet weak var imageLogin: UIImageView!
    @IBOutlet weak var usuarioTextField: UITextField!
    @IBOutlet weak var passwdTextField: UITextField!
    
    //MARK:- Actions
    @IBAction func entrarButton(_ sender: Any) {
        let user = usuarioTextField.text ?? ""
        let passwd = passwdTextField.text ?? ""
        switch (user, passwd){
        case("usuario","passwd"):
            //print("login Correcto")
            performSegue(withIdentifier: "ciudadSegue", sender: self)
        case("usuario", _):
            mostrarAlerta(mensaje: "Contraseña Incorrecta")
        default:
            mostrarAlerta(mensaje: "Usuario y Contraseña Incorrecta")
        }
    }
    
    private func mostrarAlerta (mensaje:String){
        let alertView = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: .default){(action) in
            self.usuarioTextField.text = ""
            self.passwdTextField.text = ""
        }
        alertView.addAction(aceptar)
        present(alertView, animated: true, completion: nil)
    }
    
    @IBAction func invitadoButton(_ sender: Any) {
    }
}

