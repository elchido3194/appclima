//
//  CiudadViewController.swift
//  AppClima
//
//  Created by Jose Diaz on 31/10/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import UIKit

class CiudadViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Outlets
    @IBOutlet weak var climaLabel: UILabel!
    @IBOutlet weak var ciudadTextField: UITextField!
    
    //MARK: - Actions
    @IBAction func buscarButtonPressed(_ sender: Any) {
        let service = Servicio()
        service.consultaPorCiudad(city: ciudadTextField.text!) { (weather) in
            DispatchQueue.main.async {
                self.climaLabel.text = weather
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
