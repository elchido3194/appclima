//
//  Servicio.swift
//  AppClima
//
//  Created by Jose Diaz on 7/11/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import Foundation

class Servicio {
    func consultaPorCiudad(city:String, completion:@escaping (String)-> ()){
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=e89b5f70921f3d6a46ca43c2163f3e58"
        consulta(urlString: urlString) { (weather) in
            completion(weather)
        }
    }
    
    func consultaUbicacionActual(lat:Double, lon:Double, completion:@escaping (String)->()){
        let urlString = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=e89b5f70921f3d6a46ca43c2163f3e58"
        //consulta(urlString: urlString)
        consulta(urlString: urlString) { (weather) in
            completion(weather)
        }
    }
    
    func consulta(urlString:String, completion:@escaping (String)->()){
        
        let url = URL(string: urlString)
        let urlRequest = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let _ = error {
                return
            }
            do{
                let weatherJSON = try JSONSerialization.jsonObject(with: data!, options: [])
                //print("\(weatherJSON)
                //print(type(of: weatherJSON))
                let weatherDic = weatherJSON as! NSDictionary
                //Guard intenta realizar una accion y si esta no funciona, se ejecuta dentro de las llaves
                guard let weatherKey = weatherDic ["weather"] as? NSArray else{
                    completion("Ciudad No valida")
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                //Permite regresar al hilo de ejecucion principal, ya que el task genera un hilo adicional
                completion(weather["description"] as! String)
                //self.climaLabel.text = "\(weather["description"] ?? "Error")"
                //print(weather)
                //print("--")
                //print(type(of: weather))
            }catch{
                print("Error al generar el JSON")
            }
        }
        task.resume()
        
    }
}
