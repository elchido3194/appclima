//
//  InvitadoViewController.swift
//  AppClima
//
//  Created by Jose Diaz on 25/10/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import UIKit
import CoreLocation

class InvitadoViewController: UIViewController, CLLocationManagerDelegate {
    //Instancia de la clase manager para la ubicacion
    let coreLocationMan = CLLocationManager()
    //Bandera para evitar que se actualice
    //var bandera = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Pide permiso al usuario para utilizar los servicios de ubicacion
        coreLocationMan.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            coreLocationMan.delegate = self
            coreLocationMan.startUpdatingLocation()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        //consultarUbicacionActual()
    }
    //MARK:- Outlets
    @IBOutlet weak var climaLabel: UILabel!
    //MARK:- coreLocationMan delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
        //if !bandera{
        let service = Servicio()
        service.consultaUbicacionActual(lat: (location?.latitude)!, lon: (location?.longitude)!) { (weather) in
            DispatchQueue.main.async {
                self.climaLabel.text = weather
            }
        }
        //service.consultaUbicacionActual(lat: (location?.latitude)!, lon: (location?.longitude)!)
        //bandera=true
        manager.stopUpdatingLocation()
        //print(location)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
